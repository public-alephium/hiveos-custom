#!/usr/bin/env bash

# this function is ok
function miner_ver() {

  echo $CUSTOM_VERSION
}

# this function is ok
function miner_config_echo() {
        if [[ -z $CUSTOM_MINER ]]; then
                echo -e "${RED}\$CUSTOM_MINER is not defined${NOCOLOR}"
                return 1
        fi

        if [[ -e $CUSTOM_DIR/h-manifest.conf ]]; then
                source $CUSTOM_DIR/h-manifest.conf
        fi

        if [[ ! -z $CUSTOM_CONFIG_FILENAME ]]; then
                miner_echo_config_file $CUSTOM_CONFIG_FILENAME
        else
                echo -e "${RED}\$CUSTOM_CONFIG_FILENAME is not defined${NOCOLOR}";
                return 1
        fi

}

CUSTOM_DIR=$(dirname "$BASH_SOURCE")

. $CUSTOM_DIR/h-manifest.conf

[[ -z $CUSTOM_TEMPLATE ]] && echo -e "${YELLOW}CUSTOM_TEMPLATE is empty${NOCOLOR}" && return 1
[[ -z $CUSTOM_URL ]] && echo -e "${YELLOW}CUSTOM_URL is empty${NOCOLOR}" && return 2

IFS='.' read -ra ADDR_WORKER <<< "$CUSTOM_TEMPLATE"

ADDRESSES=$(echo ${ADDR_WORKER[0]} | sed 's/,/","/ig' | sed -e 's/.*/"&"/'|tr -d "[:blank:]")

#check single addr or 4 addr
if [[ $(echo ${ADDRESSES}  | tr ',' '[\n*]' | wc -l) -eq 4 ]];
then
  ADDRESS_SECTION=" \"addresses\": [ ${ADDRESSES} ]"
else
  ADDRESS_SECTION=" \"address\": ${ADDRESSES}"
fi

IFS=':' read -ra SERVER_URL_PORT <<< "$CUSTOM_URL"

SERVER_URL=${SERVER_URL_PORT[0]}

SERVER_PORT=20032
if [[ ${SERVER_URL_PORT[1]} -ne 0 ]];then
  SERVER_PORT=${SERVER_URL_PORT[1]}
fi


cat <<EOT > "$CUSTOM_CONFIG_FILENAME"
{
    "logPath": "$CUSTOM_LOG_BASEDIR",
    "diff1TargetNumZero": 30,
    "serverHost": "${SERVER_URL}",
    "serverPort": "${SERVER_PORT}",
    "proxyPort": "30032",
    "workerName": "${ADDR_WORKER[1]}",
    $ADDRESS_SECTION

}
EOT

MINER_NAME=miners/custom
