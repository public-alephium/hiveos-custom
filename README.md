# hiveos-custom

## Wallet

**Miner wallet is mandatory** --> [How to create miner wallet](https://wiki.alephium.org/GPU-Miner-Guide.html#create-miner-wallet)


Addresses must be separated by `,`


## Custom miner

* Installation URL: [https://gitlab.com/public-alephium/hiveos-custom/-/raw/main/packages/alephium-1.34.tar.gz](https://gitlab.com/public-alephium/hiveos-custom/-/raw/main/packages/alephium-1.34.tar.gz)
* Hash algorithm: `blake3-alph`
* Wallet and worker template: `%WAL%`
* Pool URL: URL of the pool. For example: `pool.metapool.tech`

[How to install a custom miner on HiveOS](https://hiveos.farm/getting_started-start_custom_miner/)

# Configuration examples

## Wallet

<img src="./static/img/wallet.png" width="500">


## Flightsheet

<img src="./static/img/flighsheets.png" width="500">


## Custom miner

<img src="./static/img/miner.png" width="500">

