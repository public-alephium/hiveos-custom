#!/usr/bin/env bash

cd `dirname $0`

export LD_LIBRARY_PATH=.

. colors

CUSTOM_DIR=$(dirname "$BASH_SOURCE")

. $CUSTOM_DIR/h-manifest.conf

[[ -z $CUSTOM_LOG_BASEDIR ]] && echo -e "${RED}No CUSTOM_LOG_BASEDIR is set${NOCOLOR}" && exit 1
[[ -z $CUSTOM_CONFIG_FILENAME ]] && echo -e "${RED}No CUSTOM_CONFIG_FILENAME is set${NOCOLOR}" && exit 1
[[ ! -f $CUSTOM_CONFIG_FILENAME ]] && echo -e "${RED}Custom config ${YELLOW}$CUSTOM_CONFIG_FILENAME${RED} is not found${NOCOLOR}" && exit 1

mkdir -p $CUSTOM_LOG_BASEDIR
touch $CUSTOM_LOG_BASENAME.log

#source $CUSTOM_CONFIG_FILENAME



# must be in the same directory for proxy to find the config
cd $CUSTOM_DIR/proxy/${CUSTOM_VERSION_PROXY} ; alephium-mining-proxy-linux $CUSTOM_CONFIG_FILENAME &

if [ $(gpu-detect NVIDIA) -gt 0 ]; then
  $CUSTOM_DIR/${CUSTOM_VERSION}/alephium-cuda-miner-linux -p 30032 2>&1 | tee --append $CUSTOM_LOG_BASENAME.log
elif [ $(gpu-detect AMD) -gt 0 ]; then
  $CUSTOM_DIR/${CUSTOM_VERSION_AMD}/alephium-amd-miner-linux -p 30032 2>&1 | tee --append $CUSTOM_LOG_BASENAME.log
fi
