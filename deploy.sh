#!/bin/bash
VERSION=1.34
GPU_MINER_VERSION=0.5.4
GPU_MINER_VERSION_AMD=0.2.0
PROXY_VERSION=1.1.1

echo "Deploy version ${VERSION}"

rm -rf alephium
mkdir -p alephium
cp -R control h-run.sh h-stats.sh  h-config.sh h-manifest.conf ${GPU_MINER_VERSION} ${GPU_MINER_VERSION_AMD}  -t alephium
mkdir -p alephium/proxy/
cp -R proxy/${PROXY_VERSION} alephium/proxy/
tar -zcvf alephium-${VERSION}.tar.gz alephium

mv  alephium-${VERSION}.tar.gz packages

git add packages/* && git commit -a -m "deploy new code" && git push
